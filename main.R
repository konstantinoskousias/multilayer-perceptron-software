# Install libraries
library(Metrics)
library(monmlp)

# Source files
source('hindnorm.R')

# Load datasets
load('pollution.RData')
data = pollution

hindnorm(data)

training = model.matrix(~ . + 0, data = as.data.frame(training))
testing = model.matrix(~ . + 0, data = as.data.frame(testing))

fit <- monmlp.fit(as.matrix(training[,-1]), as.matrix(training[,1]), hidden1=3, n.ensemble=3, monotone=1, bag=TRUE)
predict_test <- monmlp.predict(x = as.matrix(testing[,-1]), weights = fit)

# predict_train_back = predict_train*(max(data[,1]) - min(data[,1])) + min(data[,1])
predict_test_back = predict_test*(max(data[,1]) - min(data[,1])) + min(data[,1])

# residuals_train = predict_train_back - training_def[,1]
residuals_test = predict_test_back - testing_def[,1]

mae_testing = mae(testing_def[,1], predict_test_back) # Mean absolute error (MAE)
mape_testing = mape(testing_def[,1], predict_test_back) # Mean absolute percentage error (MAPE)
